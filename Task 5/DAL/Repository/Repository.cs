﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Repository
{
    class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbSet<TEntity> _dbSet;
        private readonly DbContext _dbContext;

        public Repository(DbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dbSet = dbContext.Set<TEntity>();
        }

        public void Add(TEntity item)
        {
            _dbSet.Add(item);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            _dbSet.AddRange(entities);
        }

        public int Count()
        {
            return _dbSet.Count();
        }

        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Count(predicate);
        }

        public void Remove(int id)
        {
            var entity = Get(id);
            if (entity != null)
            {
                _dbSet.Remove(entity);
            }
        }

        public void Remove(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            _dbSet.RemoveRange(entities);
        }

        public void UpdateSingle(Expression<Func<TEntity, bool>> matchPredicate, TEntity updatedEntity)
        {
            var entityInDb = SingleOrDefault(matchPredicate);
            Update(entityInDb, updatedEntity);
        }

        public void Update(int idOfUpadtedEntity, TEntity updatedEntity)
        {
            var entityInDb = Get(idOfUpadtedEntity);
            Update(entityInDb, updatedEntity);
        }

        private void Update(TEntity entityToUpadte, TEntity updatedEntity)
        {
            if (entityToUpadte != null)
            {
                var dbEntry = _dbContext.Entry(entityToUpadte);
                var entry = _dbContext.Entry(updatedEntity);
                foreach (var itemName in dbEntry.CurrentValues.PropertyNames)
                {
                    var dbValue = dbEntry.Property(itemName).CurrentValue;
                    var value = entry.Property(itemName).CurrentValue;
                    if (!dbValue.Equals(value))
                    {
                        dbEntry.Property(itemName).CurrentValue = value;
                        dbEntry.Property(itemName).IsModified = true;
                    }
                }
            }
        }

        public TEntity Get(int id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public IEnumerable<TEntity> GetRange(Expression<Func<TEntity, int>> orderExpression, int startIndex, int count)
        {
            return _dbSet.OrderBy(orderExpression)
                .Skip(startIndex)
                .Take(count)
                .ToList();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, int>> orderExpression, int startIndex, int count)
        {
            return _dbSet.Where(predicate)
                .OrderBy(orderExpression)
                .Skip(startIndex)
                .Take(count)
                .ToList();
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.SingleOrDefault(predicate);
        }
    }
}
