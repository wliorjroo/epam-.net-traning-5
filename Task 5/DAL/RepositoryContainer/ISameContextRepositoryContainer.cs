﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.RepositoryContainer
{
    public interface ISameContextRepositoryContainer : IRepositoryContainer, IDisposable
    {
        void ContextSave();
    }
}
