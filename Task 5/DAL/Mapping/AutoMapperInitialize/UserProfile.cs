﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Mapping.Initialize
{
    class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<Model.Seller, DataSource.User>();
            CreateMap<DataSource.User, Model.Seller>();
        }
    }
}
