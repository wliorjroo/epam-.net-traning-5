﻿using DAL.Repository;
using DAL.Repository.Factory;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.UnitOfWork
{
    class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;
        private readonly IRepositoryFactory _repositoryFactory;
        private readonly ReaderWriterLockSlim _readerWriterLock;
        private bool _disposedValue = false;

        public UnitOfWork(DbContext dbContext, IRepositoryFactory repositoryFactory, ReaderWriterLockSlim readerWriterLock)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _repositoryFactory = repositoryFactory ?? throw new ArgumentNullException(nameof(repositoryFactory));
            _readerWriterLock = readerWriterLock ?? throw new ArgumentNullException(nameof(readerWriterLock));
        }

        public int Complete()
        {
            _readerWriterLock.EnterWriteLock();
            try
            {
                return _dbContext.SaveChanges();
            }
            finally
            {
                _readerWriterLock.ExitWriteLock();
            }
        }

        public IRepository<TModel> GetRepository<TModel>() where TModel : class
        {
            return _repositoryFactory.CreateRepository<TModel>();
        }

        #region IDisposable Support
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
