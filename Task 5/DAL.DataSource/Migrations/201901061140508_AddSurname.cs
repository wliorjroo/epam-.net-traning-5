namespace DAL.DataSource.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSurname : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Client", "Surname", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Client", "Surname");
        }
    }
}
