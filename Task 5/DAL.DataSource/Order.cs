namespace DAL.DataSource
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Order")]
    public partial class Order
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public int ProductCount { get; set; }

        public int ClientId { get; set; }

        public int ProductId { get; set; }

        public int UserId { get; set; }

        public virtual Client Client { get; set; }

        public virtual Product Product { get; set; }

        public virtual User User { get; set; }
    }
}
