﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebUI.Dtos
{
    public class CustomerDto
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public string Surname { get; set; }
    }
}