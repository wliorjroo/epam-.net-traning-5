﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Dtos
{
    public class OrderDto
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int ProductCount { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public int SellerId { get; set; }
    }
}