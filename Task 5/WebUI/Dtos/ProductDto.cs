﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Dtos
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }
    }
}