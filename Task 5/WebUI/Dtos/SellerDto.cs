﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Dtos
{
    public class SellerDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}