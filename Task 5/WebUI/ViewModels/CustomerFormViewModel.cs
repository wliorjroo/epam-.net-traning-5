﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebUI.Models;

namespace WebUI.ViewModels
{
    public class CustomerFormViewModel
    {
        public Customer Customer { get; set; }
    }
}