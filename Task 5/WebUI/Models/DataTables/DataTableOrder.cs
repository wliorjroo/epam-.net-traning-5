﻿namespace WebUI.Models.DataTables
{
    public class DataTableOrder
    {
        public int Column { get; set; }
        public string Dir { get; set; }
    }
}