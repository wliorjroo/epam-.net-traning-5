﻿namespace WebUI.Models.DataTables
{
    public class DataTableSearch
    {
        public string Value { get; set; }
        public bool Regex { get; set; }
    }
}