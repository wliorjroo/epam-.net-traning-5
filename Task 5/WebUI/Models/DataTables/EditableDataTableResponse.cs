﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.DataTables
{
    public class EditableDataTableResponse : DataTableResponse
    {
        public object Options { get; set; }
    }
}