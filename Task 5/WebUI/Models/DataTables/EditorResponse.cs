﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models.DataTables
{
    public class EditorResponse
    {
        public object[] Data { get; set; }
        public string Error { get; set; }
        public object[] FieldErrors { get; set; }
        public object[] Cancelled { get; set; }
    }
}