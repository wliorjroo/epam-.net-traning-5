﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using WebUI.Dtos;

namespace WebUI.Models.DataTables
{
    public class EditorModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof(EditorRequest))
            {
                return false;
            }

            var model = (EditorRequest)bindingContext.Model ?? new EditorRequest();

            model.Action = GetValue(bindingContext, "action");
            
            var orders = new List<OrderDto>();
            var keys = (bindingContext.ValueProvider as System.Web.Http.ValueProviders.Providers.CompositeValueProvider).GetKeysFromPrefix("data");
            string id;
            foreach (var key in keys)
            {
                id = GetValue(bindingContext, "data[" + key.Key + "].id");
                if (id != null)
                {
                    orders.Add(new OrderDto
                    {
                        Id = Convert.ToInt32(id),
                        Date = Convert.ToDateTime(GetValue(bindingContext, "data[" + key.Key + "].date")),
                        ProductCount = Convert.ToInt32(GetValue(bindingContext, "data[" + key.Key + "].productCount")),
                        CustomerId = Convert.ToInt32(GetValue(bindingContext, "data[" + key.Key + "].customerId")),
                        ProductId = Convert.ToInt32(GetValue(bindingContext, "data[" + key.Key + "].productId")),
                        SellerId = Convert.ToInt32(GetValue(bindingContext, "data[" + key.Key + "].sellerId")),
                    });
                }
            }
            model.Orders = orders.ToArray();

            bindingContext.Model = model;

            return true;
        }

        private string GetValue(ModelBindingContext context, string key)
        {
            var result = context.ValueProvider.GetValue(key);
            return result == null ? null : result.AttemptedValue;
        }
    }
}