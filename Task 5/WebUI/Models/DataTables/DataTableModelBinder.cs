﻿using System;
using System.Collections.Generic;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;

namespace WebUI.Models.DataTables
{
    public class DataTableModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof(DataTableRequest))
            {
                return false;
            }

            var model = (DataTableRequest)bindingContext.Model ?? new DataTableRequest();

            model.Draw = Convert.ToInt32(GetValue(bindingContext, "draw"));
            model.Start = Convert.ToInt32(GetValue(bindingContext, "start"));
            model.Length = Convert.ToInt32(GetValue(bindingContext, "length"));

            // Search
            model.Search = new DataTableSearch
            {
                Value = GetValue(bindingContext, "search.value"),
                Regex = Convert.ToBoolean(GetValue(bindingContext, "search.regex"))
            };


            // Order
            var orderIndex = 0;
            var order = new List<DataTableOrder>();
            while (GetValue(bindingContext, "order[" + orderIndex + "].column") != null)
            {
                order.Add(new DataTableOrder
                {
                    Column = Convert.ToInt32(GetValue(bindingContext, "order[" + orderIndex + "].column")),
                    Dir = GetValue(bindingContext, "order[" + orderIndex + "].dir")
                });
                orderIndex++;
            }
            model.Order = order.ToArray();

            // Columns
            var columnIndex = 0;
            var columns = new List<DataTableColumn>();
            while (GetValue(bindingContext, "columns[" + columnIndex + "].data") != null)
            {
                columns.Add(new DataTableColumn
                {
                    Data = GetValue(bindingContext, "columns[" + columnIndex + "].data"),
                    Name = GetValue(bindingContext, "columns[" + columnIndex + "].name"),
                    Orderable = Convert.ToBoolean(GetValue(bindingContext, "columns[" + columnIndex + "].orderable")),
                    Searchable = Convert.ToBoolean(GetValue(bindingContext, "columns[" + columnIndex + "].searchable")),
                    Search = new DataTableSearch
                    {
                        Value = GetValue(bindingContext, "columns[" + columnIndex + "][search].value"),
                        Regex = Convert.ToBoolean(GetValue(bindingContext, "columns[" + columnIndex + "].search.regex"))
                    }
                });
                columnIndex++;
            }
            model.Columns = columns.ToArray();

            bindingContext.Model = model;

            return true;
        }

        private string GetValue(ModelBindingContext context, string key)
        {
            var result = context.ValueProvider.GetValue(key);
            return result == null ? null : result.AttemptedValue;
        }
    }
}