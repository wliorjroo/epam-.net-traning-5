﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;
using WebUI.Dtos;

namespace WebUI.Models.DataTables
{
    [ModelBinder(typeof(EditorModelBinder))]
    public class EditorRequest
    {
        public string Action { get; set; }
        public OrderDto[] Orders { get; set; }
    }
}