﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    public static class RoleName
    {
        public const string Write = "Write";
        public const string Read = "Read";

        public static string DefaultRegistrationRole { get; } = Read;
    }
}