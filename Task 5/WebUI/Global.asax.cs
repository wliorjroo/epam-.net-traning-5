﻿using AutoMapper;
using Ninject;
using Ninject.Web.WebApi.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebUI.App_Start;

namespace WebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            // внедрение зависимостей
            //var kernel = new StandardKernel(new DI.Ninject.WebUIModule());
            //var resolver = new DI.Ninject.NinjectResolver(kernel);
            //DependencyResolver.SetResolver(resolver);
            //GlobalConfiguration.Configuration.DependencyResolver = resolver;
            //WebApiConfig.DependencyResolver = resolver;
            //kernel.Unbind<ModelValidatorProvider>();
            //kernel.Unbind<System.Web.Http.Validation.ModelValidatorProvider>();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Mapper.Initialize(c => c.AddProfile<MappingProfile>());
        }
    }
}
