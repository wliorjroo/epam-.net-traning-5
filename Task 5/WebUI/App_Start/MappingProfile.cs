﻿using AutoMapper;
using WebUI.Dtos;

namespace WebUI.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DAL.Model.Customer, Models.Customer>();
            CreateMap<DAL.Model.Product, Models.Product>();
            CreateMap<DAL.Model.Order, Models.Order>();
            CreateMap<DAL.Model.Seller, Models.Seller>();

            CreateMap<Models.Customer, DAL.Model.Customer>();
            CreateMap<Models.Product, DAL.Model.Product>();
            CreateMap<Models.Order, DAL.Model.Order>();
            CreateMap<Models.Seller, DAL.Model.Seller>();

            // Domain to Dto
            CreateMap<DAL.Model.Customer, CustomerDto>();
            CreateMap<DAL.Model.Product, ProductDto>();
            CreateMap<DAL.Model.Seller, SellerDto>();
            CreateMap<DAL.Model.Order, OrderDto>();

            // Dto to Domain
            CreateMap<CustomerDto, DAL.Model.Customer>()
                .ForMember(c => c.Id, opt => opt.Ignore());
            CreateMap<ProductDto, DAL.Model.Product>()
                .ForMember(c => c.Id, opt => opt.Ignore());
            CreateMap<SellerDto, DAL.Model.Seller>()
                .ForMember(c => c.Id, opt => opt.Ignore());
            CreateMap<OrderDto, DAL.Model.Order>()
                .ForMember(c => c.Id, opt => opt.Ignore());
        }
    }
}