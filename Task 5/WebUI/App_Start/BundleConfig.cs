﻿using System.Web;
using System.Web.Optimization;

namespace WebUI
{
    public class BundleConfig
    {
        // Дополнительные сведения об объединении см. на странице https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/lib").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/respond.js",
                        "~/scripts/bootbox.js",
                        "~/scripts/datatables/jquery.datatables.js",
                        "~/scripts/datatables/datatables.bootstrap.js",
                        "~/scripts/datatables/datatables.editor.js",
                        "~/scripts/datatables/datatables.buttons.js",
                        "~/scripts/datatables/datatables.select.js",
                        "~/scripts/datatables/editor.bootstrap.js",
                        "~/scripts/datatables/buttons.bootstrap.js",
                        "~/scripts/datatables/select.bootstrap.js",
                        "~/scripts/datatables/jquery.datatables.min.js",
                        "~/scripts/datatables/datatables.bootstrap.min.js",
                        "~/scripts/datatables/datatables.editor.min.js",
                        "~/scripts/datatables/datatables.buttons.min.js",
                        "~/scripts/datatables/datatables.select.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // готово к выпуску, используйте средство сборки по адресу https://modernizr.com, чтобы выбрать только необходимые тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js",
            //          "~/scripts/bootbox.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/content/datatables/css/datatables.bootstrap.css",
                      "~/content/datatables/css/jquery.datatables.min.css",
                      "~/content/datatables/css/editor.datatables.min.css",
                      "~/content/datatables/css/editor.bootstrap.css",
                      "~/content/datatables/css/buttons.bootstrap.css",
                      "~/content/datatables/css/select.bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
