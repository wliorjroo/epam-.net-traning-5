﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dependencies;
using System.Web.Http.ModelBinding;
using System.Web.Http.ModelBinding.Binders;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WebUI.Models.DataTables;

namespace WebUI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var settings = config.Formatters.JsonFormatter.SerializerSettings;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            settings.Formatting = Formatting.Indented;

            var dataTableProvider = new SimpleModelBinderProvider(typeof(DataTableRequest), new DataTableModelBinder());
            config.Services.Insert(typeof(ModelBinderProvider), 0, dataTableProvider);

            var editorProvider = new SimpleModelBinderProvider(typeof(EditorRequest), new EditorModelBinder());
            config.Services.Insert(typeof(ModelBinderProvider), 1, editorProvider);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
