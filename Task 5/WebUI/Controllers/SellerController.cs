﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.UnitOfWork;

namespace WebUI.Controllers
{
    [Authorize(Roles = Models.RoleName.Read)]
    public class SellerController : BaseCRUD<DAL.Model.Seller, Models.Seller>
    {
        public SellerController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        protected override DAL.Model.Seller CreateNewEntity()
        {
            return new DAL.Model.Seller();
        }

        protected override int GetId(Models.Seller model)
        {
            return model.Id;
        }
    }
}