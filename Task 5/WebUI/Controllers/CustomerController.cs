﻿using AutoMapper;
using DAL.Repository;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.ViewModels;

namespace WebUI.Controllers
{
    [Authorize(Roles = Models.RoleName.Read)]
    public class CustomerController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomerController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Customers";
            if (User.IsInRole(Models.RoleName.Write))
                return View("IndexWrite");
            return View();
        }

        [Authorize(Roles = Models.RoleName.Write)]
        public ActionResult New()
        {
            var viewModel = Mapper.Map<Models.Customer>(new DAL.Model.Customer());
            ViewBag.Title = "New Customer";
            return View("CustomerForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Models.RoleName.Write)]
        public ActionResult Save(Models.Customer customer)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Title = "Fix errors";
                return View("CustomerForm", customer);
            }

            var customerRepository = _unitOfWork.GetRepository<DAL.Model.Customer>();

            if (customer.Id == 0)
                customerRepository.Add(Mapper.Map<DAL.Model.Customer>(customer));
            else
            {
                customerRepository.UpdateSingle(c => c.Id == customer.Id, Mapper.Map<DAL.Model.Customer>(customer));
            }

            _unitOfWork.Complete();

            return RedirectToAction("Index", "Customer");
        }

        public ActionResult Details(int id)
        {
            var customerRepository = _unitOfWork.GetRepository<DAL.Model.Customer>();
            var customer = customerRepository.SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return HttpNotFound();

            var viewModel = Mapper.Map<Models.Customer>(customer);
            ViewBag.Title = "Details";
            return View(viewModel);
        }

        [Authorize(Roles = Models.RoleName.Write)]
        public ActionResult Edit(int id)
        {
            var customerRepository = _unitOfWork.GetRepository<DAL.Model.Customer>();
            var customer = customerRepository.SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return HttpNotFound();

            var viewModel = Mapper.Map<Models.Customer>(customer);
            ViewBag.Title = "Edit";
            return View("CustomerForm", viewModel);
        }
    }
}