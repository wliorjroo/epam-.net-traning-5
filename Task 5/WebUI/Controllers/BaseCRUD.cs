﻿using AutoMapper;
using DAL.Repository;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.ViewModels;

namespace WebUI.Controllers
{
    [Authorize(Roles = Models.RoleName.Read)]
    public abstract class BaseCRUD<TEntity, TModel> : Controller where TEntity : class where TModel : class
    {
        protected readonly IUnitOfWork UnitOfWork;

        protected BaseCRUD(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        public virtual ActionResult Index()
        {
            if (User.IsInRole(Models.RoleName.Write))
                return View("IndexWrite");
            return View();
        }

        [Authorize(Roles = Models.RoleName.Write)]
        public virtual ActionResult New()
        {
            var viewModel = Map(CreateNewEntity());
            ViewBag.Title = "New";
            return View("Form", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Models.RoleName.Write)]
        public ActionResult Save(TModel customer)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = customer;

                ViewBag.Title = "Fix errors";
                return View("Form", viewModel);
            }

            var customerRepository = UnitOfWork.GetRepository<TEntity>();

            if (GetId(customer) == 0)
                customerRepository.Add(Map(customer));
            else
            {
                customerRepository.Update(GetId(customer), Map(customer));
            }

            UnitOfWork.Complete();

            return RedirectToAction("Index");
        }

        public virtual ActionResult Details(int id)
        {
            var repository = UnitOfWork.GetRepository<TEntity>();
            var entity = repository.Get(id);

            if (entity == null)
                return HttpNotFound();

            var viewModel = Map(entity);

            ViewBag.Title = "Details";
            return View(viewModel);
        }

        [Authorize(Roles = Models.RoleName.Write)]
        public virtual ActionResult Edit(int id)
        {
            var repository = UnitOfWork.GetRepository<TEntity>();
            var entity = repository.Get(id);

            if (entity == null)
                return HttpNotFound();

            var viewModel = Map(entity);

            ViewBag.Title = "Edit";
            return View("Form", viewModel);
        }

        protected TEntity Map(TModel model)
        {
            return Mapper.Map<TEntity>(model);
        }

        protected TModel Map(TEntity entity)
        {
            return Mapper.Map<TModel>(entity);
        }

        protected abstract TEntity CreateNewEntity();
        
        protected abstract int GetId(TModel model);
    }
}