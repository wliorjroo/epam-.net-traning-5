﻿using DAL.Model;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebUI.Dtos;

namespace WebUI.Controllers.Api
{
    [Authorize(Roles = Models.RoleName.Read)]
    public class SellerController : BaseCRUD<Seller, SellerDto>
    {
        public SellerController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        protected override Expression<Func<Seller, bool>> BuildExpression(string query)
        {
            return c => c.Name.Contains(query) || c.Surname.Contains(query);
        }

        protected override Expression<Func<Seller, int>> BuildOrderExpression()
        {
            return e => e.Id;
        }

        //protected override void Dispose(bool disposing)
        //{
        //    UnitOfWork.Dispose();
        //}

        protected override int PutId(Seller entity, SellerDto entityDto)
        {
            return entityDto.Id = entity.Id;
        }
    }
}