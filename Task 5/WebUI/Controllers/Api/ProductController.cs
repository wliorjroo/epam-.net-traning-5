﻿using AutoMapper;
using DAL.Model;
using DAL.Repository;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebUI.Dtos;

namespace WebUI.Controllers.Api
{
    public class ProductController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Product> _repository;

        public ProductController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _repository = _unitOfWork.GetRepository<Product>();
        }

        // GET /api/product
        public IHttpActionResult GetProduct(string query = null)
        {
            IEnumerable<Product> productsQuery;

            var _repository = _unitOfWork.GetRepository<Product>();

            Expression<Func<Product, bool>> expression = c => c.Name.Contains(query);

            if (!String.IsNullOrWhiteSpace(query))
                productsQuery = _repository.Find(c => c.Name.Contains(query) || c.Cost.ToString().Contains(query));
            else
                productsQuery = _repository.GetAll();
            return Ok(productsQuery.Select(x => Mapper.Map<Product, ProductDto>(x)).ToList());
        }

        // GET /api/product/1
        public IHttpActionResult GetProduct(int id)
        {
            var _repository = _unitOfWork.GetRepository<Product>();
            var product = _repository.SingleOrDefault(c => c.Id == id);

            if (product == null)
                return NotFound();

            return Ok(Mapper.Map<Product, ProductDto>(product));
        }

        // POST /api/product
        [HttpPost]
        public IHttpActionResult CreateProduct(ProductDto productDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var product = Mapper.Map<ProductDto, Product>(productDto);
            _repository.Add(product);
            _unitOfWork.Complete();

            productDto.Id = product.Id;
            return Created(new Uri(Request.RequestUri + "/" + product.Id), productDto);
        }

        // PUT /api/product/1
        [HttpPut]
        public IHttpActionResult UpdateProduct(int id, ProductDto productDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var productInDb = _repository.SingleOrDefault(c => c.Id == id);

            if (productInDb == null)
                return NotFound();

            Mapper.Map(productDto, productInDb);

            _unitOfWork.Complete();

            return Ok();
        }

        // DELETE /api/product/1
        [HttpDelete]
        public IHttpActionResult DeleteProduct(int id)
        {
            var productInDb = _repository.SingleOrDefault(c => c.Id == id);

            if (productInDb == null)
                return NotFound();

            _repository.Remove(productInDb);
            _unitOfWork.Complete();

            return Ok();
        }
    }
}
