﻿using AutoMapper;
using DAL.Model;
using DAL.Repository;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;
using WebUI.Dtos;

namespace WebUI.Controllers.Api
{
    [Authorize(Roles = Models.RoleName.Read)]
    public class CustomerController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Customer> _repository;

        public CustomerController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _repository = _unitOfWork.GetRepository<Customer>();
        }

        // GET /api/customer
        public IHttpActionResult GetCustomer(string query = null)
        {
            IEnumerable<Customer> customersQuery;

            var _repository = _unitOfWork.GetRepository<Customer>();

            Expression<Func<Customer, bool>> expression = c => c.Name.Contains(query);

            if (!String.IsNullOrWhiteSpace(query))
                customersQuery = _repository.Find(c => c.Name.Contains(query) || c.Surname.Contains(query));
            else
                customersQuery = _repository.GetAll();
            return Ok(customersQuery.Select(x => Mapper.Map<Customer, CustomerDto>(x)).ToList());    
        }

        // GET /api/customer/1
        public IHttpActionResult GetCustomer(int id)
        {
            var _repository = _unitOfWork.GetRepository<Customer>();
            var customer = _repository.SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return NotFound();

            return Ok(Mapper.Map<Customer, CustomerDto>(customer));
        }

        // POST /api/customer
        [HttpPost]
        public IHttpActionResult CreateCustomer(CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var customer = Mapper.Map<CustomerDto, Customer>(customerDto);
            _repository.Add(customer);
            _unitOfWork.Complete();

            customerDto.Id = customer.Id;
            return Created(new Uri(Request.RequestUri + "/" + customer.Id), customerDto);
        }

        // PUT /api/customer/1
        [HttpPut]
        public IHttpActionResult UpdateCustomer(int id, CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var customerInDb = _repository.SingleOrDefault(c => c.Id == id);

            if (customerInDb == null)
                return NotFound();

            Mapper.Map(customerDto, customerInDb);

            _unitOfWork.Complete();

            return Ok();
        }

        // DELETE /api/customer/1
        [HttpDelete]
        public IHttpActionResult DeleteCustomer(int id)
        {
            var customerInDb = _repository.SingleOrDefault(c => c.Id == id);

            if (customerInDb == null)
                return NotFound();

            _repository.Remove(customerInDb);
            _unitOfWork.Complete();

            return Ok();
        }
    }
}
