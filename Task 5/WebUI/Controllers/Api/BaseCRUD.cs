﻿using AutoMapper;
using DAL.Repository;
using DAL.UnitOfWork;
using DataTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using WebUI.Models.DataTables;

namespace WebUI.Controllers.Api
{
    [Authorize(Roles = Models.RoleName.Read)]
    public abstract class BaseCRUD<TEntity, TDto> : ApiController where TEntity : class where TDto : class
    {
        protected readonly IUnitOfWork UnitOfWork;
        protected readonly IRepository<TEntity> Repository;

        protected BaseCRUD(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            Repository = UnitOfWork.GetRepository<TEntity>();
        }

        // GET /api/<controller>
        public IHttpActionResult Get(DataTableRequest request)
        {
            var result = DoGetRequest(request);
            return Ok(result);
        }

        public virtual DataTableResponse DoGetRequest(DataTableRequest request)
        {
            IEnumerable<TEntity> entityQuery;
            int count;

            var repository = UnitOfWork.GetRepository<TEntity>();

            if (!String.IsNullOrWhiteSpace(request.Search.Value))
            {
                Expression<Func<TEntity, bool>> expression = BuildExpression(request.Search.Value);
                entityQuery = repository.Find(expression, BuildOrderExpression(), request.Start, request.Length);
                count = repository.Count(expression);
            }
            else
            {
                entityQuery = repository.GetRange(BuildOrderExpression(), request.Start, request.Length);
                count = repository.Count();
            }

            var result = new DataTableResponse
            {
                Draw = request.Draw,
                RecordsTotal = count,
                RecordsFiltered = count,
                Data = entityQuery.ToArray(),
                Error = ""
            };
            return result;
        }

        // GET /api/<controller>/1
        public virtual IHttpActionResult Get(int id)
        {
            var _repository = UnitOfWork.GetRepository<TEntity>();
            var entity = _repository.Get(id);

            if (entity == null)
                return NotFound();

            return Ok(Map(entity));
        }

        // POST /api/<controller>
        [HttpPost]
        [Authorize(Roles = Models.RoleName.Write)]
        public virtual IHttpActionResult Create(TDto entityDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var entity = Map(entityDto);
            Repository.Add(entity);
            UnitOfWork.Complete();

            var id = PutId(entity, entityDto);
            return Created(new Uri(Request.RequestUri + "/" + id), entityDto);
        }

        // PUT /api/<controller>/1
        [HttpPut]
        [Authorize(Roles = Models.RoleName.Write)]
        public virtual IHttpActionResult Update(int id, TDto entityDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var repository = UnitOfWork.GetRepository<TEntity>();
            var entityInDb = repository.Get(id);

            if (entityInDb == null)
                return NotFound();

            repository.Update(id, Map(entityDto, entityInDb));

            UnitOfWork.Complete();

            return Ok();
        }

        // DELETE /api/<controller>/1
        [HttpDelete]
        [Authorize(Roles = Models.RoleName.Write)]
        public virtual IHttpActionResult Delete(int id)
        {
            var entityInDb = Repository.Get(id);

            if (entityInDb == null)
                return NotFound();

            Repository.Remove(id);
            UnitOfWork.Complete();

            return Ok();
        }

        protected TEntity Map(TDto model)
        {
            return Mapper.Map<TEntity>(model);
        }

        protected TEntity Map(TDto source, TEntity destination)
        {
            return Mapper.Map(source, destination);
        }

        protected TDto Map(TEntity entity)
        {
            return Mapper.Map<TDto>(entity);
        }

        protected abstract Expression<Func<TEntity, bool>> BuildExpression(string query);

        protected abstract int PutId(TEntity entity, TDto entityDto);

        protected abstract Expression<Func<TEntity, int>> BuildOrderExpression();
    }
}