﻿using AutoMapper;
using DAL.Model;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using WebUI.Dtos;
using WebUI.Models.DataTables;

namespace WebUI.Controllers.Api
{
    [Authorize(Roles = Models.RoleName.Read)]
    public class OrderController : ApiController
    {

        protected readonly IUnitOfWork UnitOfWork;
        public OrderController(IUnitOfWork unitOfWork) 
        {
            UnitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }
        // GET /api/<controller>
        public IHttpActionResult Get(DataTableRequest request)
        {
            var result = DoGetRequest(request);
            return Ok(result);
        }

        public virtual DataTableResponse DoGetRequest(DataTableRequest request)
        {
            IEnumerable<Order> entityQuery;
            int count;

            var repository = UnitOfWork.GetRepository<Order>();

            if (!String.IsNullOrWhiteSpace(request.Search.Value))
            {
                Expression<Func<Order, bool>> expression = BuildExpression(request.Search.Value);
                entityQuery = repository.Find(expression, BuildOrderExpression(), request.Start, request.Length);
                count = repository.Count(expression);
            }
            else
            {
                entityQuery = repository.GetRange(BuildOrderExpression(), request.Start, request.Length);
                count = repository.Count();
            }

            var sellers = UnitOfWork.GetRepository<Seller>()
                .GetAll()
                .Select(x => new { value = x.Id, label = x.Name + " " + x.Surname })
                .ToArray();
            var products = UnitOfWork.GetRepository<Product>()
                .GetAll()
                .Select(x => new { value = x.Id, label = x.Name })
                .ToArray();
            var customers = UnitOfWork.GetRepository<Customer>()
                .GetAll()
                .Select(x => new { value = x.Id, label = x.Name + " " + x.Surname })
                .ToArray();

            var result = new EditableDataTableResponse
            {
                Draw = request.Draw,
                RecordsTotal = count,
                RecordsFiltered = count,
                Data = entityQuery.ToArray(),
                Error = "",

                Options = new { sellers, customers, products }
            };

            return result;
        }

        [HttpPost]
        public IHttpActionResult Create(EditorRequest request)
        {
            var order = new Order
            {
                ProductCount = 0,
                Date = DateTime.Now,
                ProductId = 0,
                CustomerId = 0,
                SellerId = 0
            };

            var response = new EditorResponse
            {
                Data = new[] { order },
            };
            return Ok(response);
        }

        [HttpPut]
        public IHttpActionResult Update(EditorRequest request)
        {
            var repository = UnitOfWork.GetRepository<Order>();
            foreach (var order in request.Orders)
            {
                repository.Update(order.Id, Mapper.Map<Order>(order));
            }
            var response = new EditorResponse
            {
                Data = request.Orders
            };
            return Ok(response);
        }

        [HttpDelete]
        public IHttpActionResult Delete(EditorRequest request)
        {
            var repository = UnitOfWork.GetRepository<Order>();
            foreach (var order in request.Orders)
            {
                repository.Remove(Mapper.Map<Order>(order).Id);
            }

            var response = new EditorResponse();
            return Ok(response);
        }

        protected Expression<Func<Order, bool>> BuildExpression(string query)
        {
            return x => x.Customer.Name.Contains(query)
                || x.Customer.Surname.Contains(query)
                || x.Seller.Name.Contains(query)
                || x.Seller.Surname.Contains(query)
                || x.Product.Name.Contains(query);
        }

        protected Expression<Func<Order, int>> BuildOrderExpression()
        {
            return e => e.Id;
        }
    }
}