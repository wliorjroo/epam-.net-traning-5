﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Model;
using DAL.UnitOfWork;
using WebUI.Models;

namespace WebUI.Controllers
{
    [Authorize(Roles = Models.RoleName.Read)]
    public class OrderController : BaseCRUD<DAL.Model.Order, Models.Order>
    {
        public OrderController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        protected override DAL.Model.Order CreateNewEntity()
        {
            return new DAL.Model.Order
            {
                Customer = new DAL.Model.Customer(),
                Date = DateTime.Now,
                Seller = new DAL.Model.Seller(),
                Product = new DAL.Model.Product(),
                ProductCount = 1,
            };
        }

        protected override int GetId(Models.Order model)
        {
            return model.Id;
        }
    }
}