﻿using AutoMapper;
using DAL.Model;
using DAL.Repository;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Controllers
{
    [Authorize(Roles = Models.RoleName.Read)]
    public class ProductController : BaseCRUD<DAL.Model.Product, Models.Product>
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductController(IUnitOfWork unitOfWork) :base(unitOfWork)
        {
            //_unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        protected override DAL.Model.Product CreateNewEntity()
        {
            return new DAL.Model.Product();
        }

        protected override int GetId(Models.Product model)
        {
            return model.Id;
        }

        //// GET: Product
        //public ActionResult Index()
        //{
        //    return View();
        //}

        //public ActionResult New()
        //{
        //    var viewModel = Mapper.Map<Models.Product>(new DAL.Model.Product());
        //    ViewBag.Title = "New";
        //    return View("Form", viewModel);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles = Models.RoleName.Write)]
        //public ActionResult Save(Models.Product product)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        ViewBag.Title = "Fix errors";
        //        return View("Form", product);
        //    }

        //    var productRepository = _unitOfWork.GetRepository<DAL.Model.Product>();

        //    if (product.Id == 0)
        //        productRepository.Add(Mapper.Map<DAL.Model.Product>(product));
        //    else
        //    {
        //        productRepository.UpdateSingle(c => c.Id == product.Id, Mapper.Map<DAL.Model.Product>(product));
        //    }

        //    _unitOfWork.Complete();

        //    return RedirectToAction("Index");
        //}

        //public ActionResult Details(int id)
        //{
        //    var productRepository = _unitOfWork.GetRepository<DAL.Model.Product>();
        //    var product = productRepository.SingleOrDefault(c => c.Id == id);

        //    if (product == null)
        //        return HttpNotFound();

        //    ViewBag.Title = "Details";
        //    return View(Mapper.Map<Models.Product>(product));
        //}

        //[Authorize(Roles = Models.RoleName.Write)]
        //public ActionResult Edit(int id)
        //{
        //    var productRepository = _unitOfWork.GetRepository<DAL.Model.Product>();
        //    var product = productRepository.SingleOrDefault(c => c.Id == id);

        //    if (product == null)
        //        return HttpNotFound();

        //    var viewModel = Mapper.Map<Models.Product>(product);

        //    ViewBag.Title = "Edit";

        //    return View("Form", viewModel);
        //}
    }
}