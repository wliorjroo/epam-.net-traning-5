﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется следующим образом
// набора атрибутов. Измените значения этих атрибутов для изменения сведений,
// связанных с этой сборкой.
[assembly: AssemblyTitle("WebUI")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyProduct("WebUI")]
[assembly: AssemblyCopyright("© Microsoft, 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Установка значения False в параметре ComVisible делает типы в этой сборке невидимыми
// для компонентов COM.  Если требуется обратиться к типу в этой сборке через
// COM, задайте атрибуту ComVisible значение true для требуемого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для ID библиотеки типов typelib, если этот проект видим для COM
[assembly: Guid("d3c29b41-88b6-4b95-b5ac-922d55aa025b")]

// Сведения о версии сборки состоят из указанных ниже четырех значений:
//
//      основной номер версии;
//      Дополнительный номер версии
//      номер сборки;
//      редакция.
//
// Можно задать все значения или принять номер редакции и номер сборки по умолчанию,
// используя "*", как показано ниже:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
