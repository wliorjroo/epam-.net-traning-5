﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.DI.Ninject
{
    public class NinjectResolver : NinjectScope, System.Web.Http.Dependencies.IDependencyResolver, System.Web.Mvc.IDependencyResolver
    {
        private IKernel _kernel;

        public NinjectResolver(IKernel kernel) : base(kernel)
        {
            _kernel = kernel ?? throw new ArgumentNullException(nameof(kernel));
        }

        public System.Web.Http.Dependencies.IDependencyScope BeginScope()
        {
            //INinjectSettings settings = new NinjectSettings { LoadExtensions = false };
            //var block =(_kernel as StandardKernel).BeginBlock().
            //var block = _kernel.BeginBlock();
            //foreach(var item in block.Release
            return new NinjectScope(_kernel.BeginBlock());
        }
    }
}