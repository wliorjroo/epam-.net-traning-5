﻿using Ninject;
using Ninject.Syntax;
using Ninject.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.DI.Ninject
{
    class DependencyResolver : NinjectDependencyResolver
    {
        public DependencyResolver(IKernel kernel) : base(kernel)
        {
            kernel.Unbind<ModelValidatorProvider>();
            kernel.Load(new DAL.DI.Ninject.DALModule());
        }
    }
}