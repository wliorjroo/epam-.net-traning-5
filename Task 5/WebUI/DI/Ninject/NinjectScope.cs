﻿using Ninject;
using Ninject.Activation;
using Ninject.Activation.Blocks;
using Ninject.Parameters;
using Ninject.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;

namespace WebUI.DI.Ninject
{
    public class NinjectScope : IDependencyScope
    {
        private IResolutionRoot _resolutionRoot;

        public NinjectScope(IResolutionRoot resolutionRoot)
        {
            _resolutionRoot = resolutionRoot ?? throw new ArgumentNullException(nameof(resolutionRoot));
        }

        public object GetService(Type serviceType)
        {
            if (_resolutionRoot == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");

            //IRequest request = _resolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true, true);
            return _resolutionRoot.Get(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (_resolutionRoot == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");

            //IRequest request = _resolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true, true);
            return _resolutionRoot.GetAll(serviceType);//_resolutionRoot.Resolve(request).ToList();
        }

        public void Dispose()
        {
            IDisposable disposable = _resolutionRoot as IDisposable;
            if (disposable != null)
                disposable.Dispose();

            _resolutionRoot = null;
        }
    }
}