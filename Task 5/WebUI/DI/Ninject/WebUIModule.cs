﻿using AutoMapper;
using DAL.Repository;
using DAL.Repository.Factory;
using DAL.RepositoryContainer;
using DAL.UnitOfWork;
using Ninject;
using Ninject.Modules;
//using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Validation;
using System.Web.Http.Validation.Providers;

namespace WebUI.DI.Ninject
{
    public class WebUIModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Load(new[] { new DAL.DI.Ninject.DALModule() });

            //Kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel).InSingletonScope();
            //Kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>().InSingletonScope();
        }
    }
}
