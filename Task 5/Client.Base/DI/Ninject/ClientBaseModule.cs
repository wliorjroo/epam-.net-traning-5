﻿using BL.DI.Ninject;
using Client.Base.Configuration;
using Client.Base.Parameters;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Base.DI.Ninject
{
    public class ClientBaseModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Load(new[] { new BLModule() });

            Bind<ISettings>().To<Settings>().InSingletonScope();
            Bind<IParamParser>().To<ParamParser>().InSingletonScope();
        }
    }
}
