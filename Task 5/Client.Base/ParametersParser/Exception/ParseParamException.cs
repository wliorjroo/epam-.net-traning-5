﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Base.Parameters.Parser.Exceptions
{
    public class ParseParamException : Exception
    {
        public ParseParamException()
        {
        }

        public ParseParamException(string message) : base(message)
        {
        }
    }
}
