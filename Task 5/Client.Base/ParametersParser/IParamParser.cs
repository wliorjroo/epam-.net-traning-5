﻿using System.IO;

namespace Client.Base.Parameters
{
    public interface IParamParser
    {
        DirectoryInfo FolderPath { get; }
        DirectoryInfo NotParsedPath { get; }
        DirectoryInfo ParsedPath { get; }

        void Parse(string[] args);
    }
}