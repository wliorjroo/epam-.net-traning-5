﻿using Client.Base.Configuration;
using Client.Base.Parameters.Parser.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Base.Parameters
{
    class ParamParser : IParamParser
    {
        private readonly Settings _settings;

        public ParamParser()
        {
            _settings = new Settings();
        }

        public DirectoryInfo FolderPath { get; private set; }
        public DirectoryInfo ParsedPath { get; private set; }
        public DirectoryInfo NotParsedPath { get; private set; }

        public void Parse(string[] args)
        {
            if (args.Length == 3)
            {
                if (Directory.Exists(args[0]) && Directory.Exists(args[1]) && Directory.Exists(args[2]))
                {
                    FolderPath = new DirectoryInfo(args[0]);
                    ParsedPath = new DirectoryInfo(args[1]);
                    NotParsedPath = new DirectoryInfo(args[2]);
                }
                else
                {
                    throw new ParseParamException("Incorrect folder path!");
                }
            }
            else if (args.Length > 0)
            {
                throw new ParseParamException("Incorrect param string!");
            }
            else // args.Length == 0
            {
                FolderPath = new DirectoryInfo(_settings.GetFolderPath());
                ParsedPath = new DirectoryInfo(_settings.GetParsedPath());
                NotParsedPath = new DirectoryInfo(_settings.GetNotParsedPath());
            }
        }
    }
}
