﻿using BL.FileReader;
using BL.Parser.Exception;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BL.CSVReader
{
    abstract class CSVParallelReader : IDisposable
    {
        protected readonly string WatchFilesExtention;
        protected readonly IFileReader FileReader;
        protected CancellationTokenSource TokenSource;
        private bool disposedValue = false;

        public CSVParallelReader(IFileReader fileReader, string watchFilesExtention)
        {
            FileReader = fileReader ?? throw new ArgumentNullException(nameof(fileReader));
            WatchFilesExtention = watchFilesExtention ?? throw new ArgumentNullException(nameof(watchFilesExtention));
        }

        protected Task[] ParallelRead(FileInfo[] files, DirectoryInfo parsedFilesPath, DirectoryInfo notParsedFilesPath)
        {
            TokenSource = new CancellationTokenSource();
            Task[] taskArray = new Task[files.Length];
            for (int i = 0; i < files.Length; i++)
            {
                taskArray[i] = Task.Factory.StartNew(ReadFile, new TaskParams(files[i], parsedFilesPath, notParsedFilesPath), TokenSource.Token);
            }
            return taskArray;
        }

        private void ReadFile(object objectParam)
        {
            TokenSource.Token.ThrowIfCancellationRequested();
            TaskParams taskParams = (TaskParams)objectParam;
            FileInfo file = taskParams.File;
            DirectoryInfo parsedFilesPath = taskParams.ParsedFilesPath;
            DirectoryInfo notParsedFilesPath = taskParams.NotParsedFilesPath;

            try
            {
                TokenSource.Token.ThrowIfCancellationRequested();
                FileReader.ReadFile(file.FullName);
                var destination = GetDestinationFileName(parsedFilesPath.FullName, file.Name);
                file.MoveTo(destination);
            }
            catch (ParseException)
            {
                var destination = GetDestinationFileName(notParsedFilesPath.FullName, file.Name);
                file.MoveTo(destination);
            }
        }

        private string GetDestinationFileName(string directory, string fileName)
        {
            var result = string.Format(@"{0}{1}_parsed_at_{2}{3}", directory, fileName.Substring(0, fileName.LastIndexOf('.')), DateTime.Now.ToString("yyyyMMdd_HHmmss"), WatchFilesExtention);
            return result;
        }

        #region IDisposable Support
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    TokenSource.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
