﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.CSVReader
{
    class TaskParams
    {
        public readonly FileInfo File;
        public readonly DirectoryInfo ParsedFilesPath;
        public readonly DirectoryInfo NotParsedFilesPath;

        public TaskParams(FileInfo file, DirectoryInfo parsedFilesPath, DirectoryInfo notParsedFilesPath)
        {
            File = file;
            ParsedFilesPath = parsedFilesPath;
            NotParsedFilesPath = notParsedFilesPath;
        }
    }
}
