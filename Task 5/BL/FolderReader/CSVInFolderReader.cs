﻿using BL.FileReader;
using BL.Parser.Exception;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BL.FolderReader
{
    class CSVInFolderReader : CSVReader.CSVParallelReader, IFolderReader
    {
        private const string WatchFilesExtension = ".csv";
        private const string FilesPattern = "*.csv";

        public CSVInFolderReader(IFileReader fileReader) : base(fileReader, WatchFilesExtension)
        {
        }

        public void ReadFiles(DirectoryInfo readDirectory, DirectoryInfo parsedFilesDirectory, DirectoryInfo notParsedFilesDirectory)
        {
            FileInfo[] readFileInfo = readDirectory.GetFiles(FilesPattern);
            Task[] taskArray = ParallelRead(readFileInfo, parsedFilesDirectory, notParsedFilesDirectory);
            try
            {
                Task.WaitAll(taskArray);
            }
            catch (AggregateException)
            {
                // TODO exception handling
            }
        }
    }
}
