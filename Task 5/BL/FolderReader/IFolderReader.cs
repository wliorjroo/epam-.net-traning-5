﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.FolderReader
{
    public interface IFolderReader : IDisposable
    {
        void ReadFiles(DirectoryInfo readDirectory, DirectoryInfo parsedFilesDirectory, DirectoryInfo notParsedFilesDirectory);
    }
}
