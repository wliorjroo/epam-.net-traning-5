﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Parser.Exception
{
    public class ParseException : System.Exception
    {
        public ParseException()
        {
        }

        public ParseException(string message) : base(message)
        {
        }
    }
}
