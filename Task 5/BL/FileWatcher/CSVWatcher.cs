﻿using BL.FileReader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace BL.FileWatcher
{
    class CSVWatcher : CSVReader.CSVParallelReader, IFileWatcher
    {
        private const string _watchFilesPattern = "*.csv";
        private const string _watchFilesExtention = ".csv";
        private readonly FileSystemWatcher _watcher;
        private DirectoryInfo _parsedFilesDirectory;
        private DirectoryInfo _notParsedFilesDirectory;
        private bool disposedValue = false;


        public bool IsWatching { get; private set; }

        public CSVWatcher(IFileReader fileReader) : base(fileReader, _watchFilesExtention)
        {
            _watcher = new FileSystemWatcher();
            IsWatching = false;
            _watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName; 
            _watcher.Filter = _watchFilesPattern;
            _watcher.Created += _onCreate;
        }

        public void BeginWatch(DirectoryInfo readDirectory, DirectoryInfo parsedFilesDirectory, DirectoryInfo notParsedFilesDirectory)
        {
            if (IsWatching)
            {
                throw new InvalidOperationException();
            }

            IsWatching = true;
            _watcher.Path = readDirectory.FullName;
            _parsedFilesDirectory = parsedFilesDirectory;
            _notParsedFilesDirectory = notParsedFilesDirectory;
            _watcher.EnableRaisingEvents = true;
            TokenSource = new System.Threading.CancellationTokenSource();
        }

        public void EndWatch()
        {
            if (!IsWatching)
            {
                throw new InvalidOperationException();
            }

            IsWatching = false;
            _watcher.EnableRaisingEvents = false;
            _watcher.Changed -= _onCreate;
            TokenSource.Cancel();
        }

        private void _onCreate(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Created)
            {
                FileInfo[] readFile = new[] { new FileInfo(e.FullPath) };
                Task[] taskArray = ParallelRead(readFile, _parsedFilesDirectory, _notParsedFilesDirectory);
                try
                {
                    Task.WaitAll(taskArray);
                }
                catch (AggregateException)
                {
                    // TODO exception handling
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (IsWatching)
                    {
                        EndWatch();
                    }
                    _watcher.Dispose();
                    base.Dispose(disposing);
                }

                disposedValue = true;
            }
        }
    }
}
